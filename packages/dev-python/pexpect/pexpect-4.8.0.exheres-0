# Copyright 2009, 2021 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require utf8-locale pypi setup-py [ import=distutils test=pytest ]

SUMMARY="Allows easy control of interactive console applications"
DESCRIPTION="
Pexpect is a Python module for spawning child applications and controlling them
automatically. Pexpect can be used for automating interactive applications such
as ssh, ftp, passwd, telnet, etc. It can be used to a automate setup scripts for
duplicating software package installations on different servers. It can be used
for automated software testing. Pexpect is in the spirit of Don Libes' Expect,
but Pexpect is pure Python. Other Expect-like modules for Python require TCL
and Expect or require C extensions to be compiled. Pexpect does not use C,
Expect, or TCL extensions. It should work on any platform that supports the
standard Python pty module. The Pexpect interface focuses on ease of use so
that simple tasks are easy.
"

LICENCES="ISC"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# Test dependencies differs from declared in package:
# pytest-capturelog has been merged into the core of pytesta,
# coverage and unpackaged coveralls are also listed, but not really required.
DEPENDENCIES="
    build+run:
        dev-python/ptyprocess[>=0.5][python_abis:*(-)?]
"

PYTHON_BYTECOMPILE_EXCLUDES=(
    pexpect/_async.py  # incompatible with py27
)

PYTEST_PARAMS=(
    -v
    -k "not InteractTestCase and not test_bash"  # fails under Paludis and/or on CI
)

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/${PNV}-sydbox.patch )

pkg_setup() {
    require_utf8_locale  # for tests on py27
}

