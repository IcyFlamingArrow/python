# Copyright 2018 Alexander Kapshuna <kapsh@kap.sh>
# Distributed under the terms of the GNU General Public License v2

require pypi
require setup-py [ import=setuptools test=pytest ]

SUMMARY="Provides an uniform layer to support PyQt5/PySide2 with a single codebase"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    providers: ( pyqt5 pyside2 ) [[
        number-selected = at-least-one
    ]]
"

DEPENDENCIES="
    build+run:
        providers:pyqt5? ( dev-python/PyQt5[python_abis:*(-)?] )
        providers:pyside2? ( dev-python/PySide2[python_abis:*(-)?] )
"

# Tests are mostly testing availability of PyQt5 parts.
# Restrict them as this heavily depends on enabled PyQt options.
RESTRICT="test"

pkg_postinst() {
    einfo "${PN} uses first found provider in order of preference: pyqt5, pyside2"
    einfo "To override this, environment variable QT_API can be set to one of these names"
}

